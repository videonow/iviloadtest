// Used by ../../../../tools/browsers-binaries-standalone/ 
// to pre-install required browsers to run tests

const path = require("path");

module.exports = {
    firefox: {
        version: "47.0.1",
        platform: "Win_x64"
    },
    defaultPath: path.join(__dirname, "../browsers")
};
